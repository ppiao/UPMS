#通用用户权限系统(UPMS)-最灵活的用户权限管理系统
作者：Yonggang.Liu
论坛: http://f.sutaitech.com
个人网站：http://www.lyg945.com
版本：0.0.1-SNAPSHOT

#技术
框架采用 springMvc + 前端模板 beetl + 数据库操作 beetlsql 

#beetlsql 特点   http://git.oschina.net/xiandafu
BeetSql是一个全功能DAO工具， 同时具有Hibernate 优点 & Mybatis优点功能，适用于承认以SQL为中心，同时又需求工具能自动能生成大量常用的SQL的应用。
无需注解，自动生成大量内置SQL，轻易完成增删改查功能
数据模型支持Pojo，也支持Map/List这种快速模型，也支持混合模型
SQL 以更简洁的方式，Markdown方式集中管理，同时方便程序开发和数据库SQL调试。
SQL 模板基于Beetl实现，更容易写和调试，以及扩展
简单支持关系映射而不引入复杂的OR Mapping概念和技术。
具备Interceptor功能，可以调试，性能诊断SQL，以及扩展其他功能
内置支持主从数据库，通过扩展，可以支持更复杂的分库分表逻辑
支持跨数据库平台，开发者所需工作减少到最小

#Beetl  http://www.ibeetl.com/community/?/explore/
Beetl目前版本是2.2.5,大小700K。相对于其他java模板引擎，具有功能齐全，语法直观,性能超高，以及编写的模板容易维护等特点。使得开发和维护模板有很好的体验。是新一代的模板引擎。总得来说，它的特性如下：
功能完备：作为主流模板引擎，Beetl具有相当多的功能和其他模板引擎不具备的功能。适用于*各种应用场景*，从对响应速度有很高要求的大网站到功能繁多的CMS管理系统都适合。Beetl本身还具有很多独特功能来完成模板编写和维护，这是其他模板引擎所不具有的。
非常简单：类似Javascript语法和习俗，只要半小时就能通过半学半猜完全掌握用法。拒绝其他模板引擎那种非人性化的语法和习俗。同时也能支持html 标签，使得开发CMS系统比较容易
超高的性能：Beetl 远超过主流java模板引擎性能(引擎性能5-6倍与freemaker，2倍于JSP。参考附录），而且消耗较低的CPU
易于整合：Beetl能很容易的与各种web框架整合，如Spring MVC，JFinal,Struts,Nutz，Jodd，Servlet等。
支持模板单独开发和测试，即在MVC架构中，即使没有M和C部分，也能开发和测试模板。
扩展和个性化：Beetl支持自定义方法，格式化函数，虚拟属性，标签，和HTML标签. 同时Beetl也支持自定义占位符和控制语句起始符号也支持使用者完全可以打造适合自己的工具包.

感谢 闲大赋（Gavin·King） 提供支持，让我的开发轻松了许多

![输入图片说明](http://git.oschina.net/uploads/images/2015/1013/171209_da1b46e6_91544.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2015/1013/171219_819e5f89_91544.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2015/1013/171240_c77d75ec_91544.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2015/1013/171249_2f2c09d6_91544.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2015/1013/171256_e513b61f_91544.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2015/1013/171302_e3891a1f_91544.png "在这里输入图片标题")